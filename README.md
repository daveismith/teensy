teensy-freertos
===================

This repository contains the base project for teensy 3.1 projects built upon FreeRTOS.

dependencies
=============
1. [gcc-arm](https://launchpad.net/gcc-arm-embedded) - minimum 4.8.3 20140228
2. [FreeRTOS](http://www.freertos.org/) - version 8.1.2 (included)
3. make

building
=========

1. make
